<?php
/**
 * Created by PhpStorm.
 * User: RISHOUBA
 * Date: 27/03/2018
 * Time: 19:19
 */

namespace App\Controller\eleve;

use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\valeurAll;
use App\Form\StageType;
use App\Form\TuteurType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class EleveController extends controller
{

    /**
     * @Route("/eleve/home", name="elevefHome")
     */
    public function home()
    {
        return $this->render('eleve/home.html.twig', [
            'controller_name' => 'EleveController',
        ]);
    }


    /**
     * @Route("eleve/entreprises", name="viewEntreprisesEleve")
     */
    public function viewEntreprises()
    {
        $entreprises = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();

        return $this->render('eleve/entreprises.html.twig', [
            'entreprises' => $entreprises
        ]);
    }


    /**
     * @Route("eleve/entreprise", name="ajout_entreprise")
     */
    public function AjoutEntreprise(Request $request)
    {


        $item = new Entreprise();
        $item->setNomEntreprise('');
        $item->setVilleEntreprise('');
        $item->setCpEntreprise(0);
        $item->setAdresseEntreprise('');
        $item->setMailEntreprise('');
        $item->setTelEntreprise('');
        $item->setActiviteEntreprise('');
        $item->setActive(0);

        $form = $this->createFormBuilder($item)
            ->add('nomEntreprise', TextType::class)
            ->add('villeEntreprise', TextType::class)
            ->add('cpEntreprise', TextType::class)
            ->add('adresseEntreprise', TextType::class)
            ->add('mailEntreprise', EmailType::class)
            ->add('telEntreprise', TextType::class)
            ->add('activiteEntreprise', TextType::class)
            ->add('active', CheckboxType::class)
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $item = $form->getData();
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($item);
                    $em->flush();
                    return $this->redirectToRoute('viewEntreprisesEleve');
                }
            }
        }
        return $this->render('eleve/ajoutEntreprise.html.twig', array(
            'form' => $form->createView(),
            )
        );

    }

    /**
     * @Route("eleve/stage", name="ajout_stage")
     */
    public  function  ajoutStage (Request $request)
    {
        $item = new Stage();
        $form=$this->createForm(StageType::class,$item);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            $em= $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();

            return $this->redirect('entreprise');
        }
        return $this->render('eleve/ajoutStage.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("eleve/tuteur", name="ajout_tuteur")
     */
    public  function  ajoutTuteur (Request $request)
    {
        $item = new Tuteur();
        $form=$this->createForm(TuteurType::class,$item);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            $em= $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();

            return $this->redirect('entreprise');
        }
        return $this->render('eleve/ajoutTuteur.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }



}