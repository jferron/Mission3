<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\Usereleve;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ProfController extends Controller
{

    /**
     * @Route("/prof/home", name="profHome")
     */
    public function home()
    {
        return $this->render('prof/home.html.twig', [
            'controller_name' => 'ProfController',
        ]);
    }

    /**
     * @Route("/prof/lists", name="profLists")
     */
    public function lists()
    {
        return $this->render('prof/lists.html.twig', [
            'controller_name' => 'ProfController',
        ]);
    }

    /**
     * @Route("/prof/help", name="profHelp")
     */
    public function help()
    {
        return $this->render('prof/help.html.twig', [
            'controller_name' => 'ProfController',
        ]);
    }

    /**
     * @Route("/prof/lists/sansStages", name="listsEleveSansStage")
     */
    public function eleveSansStage()
    {
        $sansStages = $this->getDoctrine()
            ->getRepository(Usereleve::class)
            ->findElevesSansStages();

        return $this->render('prof/sansStages.html.twig', [
            'sansStages' => $sansStages
        ]);
    }

    /**
     * @Route("/prof/lists/avecStages", name="listsElevesAvecStage")
     */
    public function eleveAvecStage()
    {
        $avecStages = $this->getDoctrine()
            ->getRepository(Usereleve::class)
            ->findElevesAvecStages();

        return $this->render('prof/avecStages.html.twig', [
            'avecStages' => $avecStages
        ]);
    }

    /**
     * @Route("/prof/lists/nonSuivis", name="listsElevesNonSuivis")
     */
    public function eleveStageNonSuivi()
    {
        $nonSuivis = $this->getDoctrine()
            ->getRepository(Usereleve::class)
            ->findElevesStagesNonSuivis();

        return $this->render('prof/nonSuivis.html.twig', [
            'nonSuivis' => $nonSuivis
        ]);
    }

    /**
     * @Route("/prof/help/pdfeleve", name="pdfeleve")
     **/
    public function downloadElevesPdf()
    {
        $response = new BinaryFileResponse('/home/user/html/symfony/Mission3/public/assets/pdf/eleve.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'eleve.pdf');
        return $response;
    }

    /**
     * @Route("/prof/help/pdfprof", name="pdfprof")
     **/
    public function downloadProfPdf()
    {
        $response = new BinaryFileResponse('/home/user/html/symfony/Mission3/public/assets/pdf/prof.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'prof.pdf');
        return $response;
    }


}