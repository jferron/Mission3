<?php

namespace App\Form;

use App\Entity\Entreprise;
use App\Entity\Tuteur;
use App\Entity\Stage;
use App\Entity\Usereleve;
use App\Entity\Userprof;
use App\Entity\UserProfile;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStage')
            ->add('eleve', EntityType::class, array('class'=> Usereleve::class, 'choice_label'=>'nomEleve'))
            ->add('prof', EntityType::class, array('class'=> Userprof::class, 'choice_label'=>'nomProf'))
            ->add('tuteur', EntityType::class, array('class'=> Tuteur::class, 'choice_label'=>'nomTuteur'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stage::class,
        ]);
    }
}
