<?php

namespace App\Repository;

use App\Entity\Usereleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Usereleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usereleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usereleve[]    findAll()
 * @method Usereleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsereleveRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Usereleve::class);
    }

//    /**
//     * @return Usereleve[] Returns an array of Usereleve objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Usereleve
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findElevesSansStages() :array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM usereleve WHERE idUserEleve NOT IN (SELECT idUserEleve FROM stage);';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function findElevesAvecStages() :array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM usereleve, userprof, entreprise, tuteur, stage 
                WHERE usereleve.idUserEleve = stage.idUserEleve 
                AND userprof.idUserProf = stage.idUserProf 
                AND entreprise.idEntreprise = tuteur.idEntreprise 
                AND tuteur.idTuteur = stage.idTuteur 
                AND usereleve.idUserEleve IN (SELECT stage.idUserEleve FROM stage);';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function findElevesStagesNonSuivis() :array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM usereleve, userprof, entreprise, tuteur, stage WHERE usereleve.idUserEleve = stage.idUserEleve AND userprof.idUserProf = stage.idUserProf AND entreprise.idEntreprise = tuteur.idEntreprise AND tuteur.idTuteur = stage.idTuteur AND userprof.role =\'fantome\';';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }
}
