<?php

namespace App\Repository;

use App\Entity\Userprof;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Userprof|null find($id, $lockMode = null, $lockVersion = null)
 * @method Userprof|null findOneBy(array $criteria, array $orderBy = null)
 * @method Userprof[]    findAll()
 * @method Userprof[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserprofRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Userprof::class);
    }
    public function updateStage ()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql ='UPDATE nomEleve, prenomEleve, classeEleve, anneeScolaire, ';



    }
    public function findElevesAvecStages() :array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM usereleve, userprof, entreprise, tuteur, stage WHERE usereleve.idUserEleve = stage.idUserEleve AND userprof.idUserProf = stage.idUserProf AND entreprise.idEntreprise = tuteur.idEntreprise AND tuteur.idTuteur = stage.idTuteur AND usereleve.idUserEleve IN (SELECT stage.idUserEleve FROM stage);';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

//    /**
//     * @return Userprof[] Returns an array of Userprof objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Userprof
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
