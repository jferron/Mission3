<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180327074058 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stage CHANGE idStage idStage INT AUTO_INCREMENT NOT NULL, CHANGE idUserEleve idUserEleve INT DEFAULT NULL, CHANGE idUserProf idUserProf INT DEFAULT NULL, CHANGE idTuteur idTuteur INT DEFAULT NULL, ADD PRIMARY KEY (idStage)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369F62E3D77 FOREIGN KEY (idUserEleve) REFERENCES usereleve (idUserEleve)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369E568DAF FOREIGN KEY (idUserProf) REFERENCES userprof (idUserProf)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936935508AF2 FOREIGN KEY (idTuteur) REFERENCES tuteur (idTuteur)');
        $this->addSql('CREATE INDEX IDX_C27C9369F62E3D77 ON stage (idUserEleve)');
        $this->addSql('CREATE INDEX IDX_C27C9369E568DAF ON stage (idUserProf)');
        $this->addSql('CREATE INDEX IDX_C27C936935508AF2 ON stage (idTuteur)');
        $this->addSql('ALTER TABLE entreprise CHANGE telEntreprise telEntreprise VARCHAR(255) DEFAULT NULL, CHANGE active active TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE userprof CHANGE role role VARCHAR(255) DEFAULT NULL, CHANGE present present TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE usereleve CHANGE role role VARCHAR(255) DEFAULT NULL, CHANGE present present TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE tuteur MODIFY idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur CHANGE telTuteur telTuteur VARCHAR(255) DEFAULT NULL, CHANGE idEntreprise idEntreprise INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (idTuteur)');
        $this->addSql('ALTER TABLE tuteur RENAME INDEX fk_tuteur_entreprise1_idx TO IDX_564122688FEDE48A');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise CHANGE telEntreprise telEntreprise VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, CHANGE active active TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE stage MODIFY idStage INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369F62E3D77');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369E568DAF');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936935508AF2');
        $this->addSql('DROP INDEX IDX_C27C9369F62E3D77 ON stage');
        $this->addSql('DROP INDEX IDX_C27C9369E568DAF ON stage');
        $this->addSql('DROP INDEX IDX_C27C936935508AF2 ON stage');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage CHANGE idStage idStage INT NOT NULL, CHANGE idUserEleve idUserEleve INT NOT NULL, CHANGE idUserProf idUserProf INT NOT NULL, CHANGE idTuteur idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur MODIFY idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur CHANGE telTuteur telTuteur VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, CHANGE idEntreprise idEntreprise INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (idTuteur, idEntreprise)');
        $this->addSql('ALTER TABLE tuteur RENAME INDEX idx_564122688fede48a TO fk_Tuteur_Entreprise1_idx');
        $this->addSql('ALTER TABLE usereleve CHANGE role role VARCHAR(200) DEFAULT \'eleve\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE userprof CHANGE role role VARCHAR(200) DEFAULT \'prof\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\'');
    }
}
