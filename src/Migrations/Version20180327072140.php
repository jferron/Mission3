<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180327072140 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE stage (idStage INT AUTO_INCREMENT NOT NULL, dateStage DATE DEFAULT NULL, idUserEleve INT DEFAULT NULL, idUserProf INT DEFAULT NULL, idTuteur INT DEFAULT NULL, INDEX IDX_C27C9369F62E3D77 (idUserEleve), INDEX IDX_C27C9369E568DAF (idUserProf), INDEX IDX_C27C936935508AF2 (idTuteur), PRIMARY KEY(idStage)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entreprise (idEntreprise INT AUTO_INCREMENT NOT NULL, nomEntreprise LONGTEXT DEFAULT NULL, villeEntreprise LONGTEXT DEFAULT NULL, cpEntreprise INT DEFAULT NULL, adresseEntreprise LONGTEXT DEFAULT NULL, mailEntreprise LONGTEXT DEFAULT NULL, telEntreprise VARCHAR(255) DEFAULT NULL, activiteEntreprise LONGTEXT DEFAULT NULL, active TINYINT(1) DEFAULT NULL, PRIMARY KEY(idEntreprise)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE userprof (idUserProf INT AUTO_INCREMENT NOT NULL, nomProf LONGTEXT DEFAULT NULL, prenomProf LONGTEXT DEFAULT NULL, login LONGTEXT DEFAULT NULL, password LONGTEXT DEFAULT NULL, role VARCHAR(255) DEFAULT NULL, present TINYINT(1) DEFAULT NULL, PRIMARY KEY(idUserProf)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usereleve (idUserEleve INT AUTO_INCREMENT NOT NULL, nomEleve LONGTEXT DEFAULT NULL, prenomEleve LONGTEXT DEFAULT NULL, classeEleve INT DEFAULT NULL, anneeScolaire LONGTEXT DEFAULT NULL, login LONGTEXT DEFAULT NULL, password LONGTEXT DEFAULT NULL, role VARCHAR(255) DEFAULT NULL, present TINYINT(1) DEFAULT NULL, PRIMARY KEY(idUserEleve)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tuteur (idTuteur INT AUTO_INCREMENT NOT NULL, nomTuteur LONGTEXT DEFAULT NULL, prenomTuteur LONGTEXT DEFAULT NULL, mailTuteur LONGTEXT DEFAULT NULL, telTuteur VARCHAR(255) DEFAULT NULL, idEntreprise INT DEFAULT NULL, INDEX IDX_564122688FEDE48A (idEntreprise), PRIMARY KEY(idTuteur)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369F62E3D77 FOREIGN KEY (idUserEleve) REFERENCES usereleve (idUserEleve)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369E568DAF FOREIGN KEY (idUserProf) REFERENCES userprof (idUserProf)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936935508AF2 FOREIGN KEY (idTuteur) REFERENCES tuteur (idTuteur)');
        $this->addSql('ALTER TABLE tuteur ADD CONSTRAINT FK_564122688FEDE48A FOREIGN KEY (idEntreprise) REFERENCES entreprise (idEntreprise)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tuteur DROP FOREIGN KEY FK_564122688FEDE48A');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369E568DAF');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369F62E3D77');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936935508AF2');
        $this->addSql('DROP TABLE stage');
        $this->addSql('DROP TABLE entreprise');
        $this->addSql('DROP TABLE userprof');
        $this->addSql('DROP TABLE usereleve');
        $this->addSql('DROP TABLE tuteur');
    }
}
