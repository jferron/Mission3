<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsereleveRepository")
 */
class Usereleve
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="idUserEleve", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nomEleve", type="text", nullable=true)
     */
    private $nomEleve;

    /**
     * @ORM\Column(name="prenomEleve", type="text", nullable=true)
     */
    private $prenomEleve;

    /**
     * @ORM\Column(name="classeEleve", type="integer", nullable=true)
     */
    private $classeEleve;

    /**
     * @ORM\Column(name="anneeScolaire", type="text", nullable=true)
     */
    private $anneeScolaire;

    /**
     * @ORM\Column(name="login", type="text", nullable=true)
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="text", nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(name="role", type="string", length=255, nullable=true)
     */
    private $role;

    /**
     * @ORM\Column(name="present", type="boolean", nullable=true)
     */
    private $present;

    public function getId()
    {
        return $this->id;
    }

    public function getNomEleve(): ?string
    {
        return $this->nomEleve;
    }

    public function setNomEleve(?string $nomEleve): self
    {
        $this->nomEleve = $nomEleve;

        return $this;
    }

    public function getPrenomEleve(): ?string
    {
        return $this->prenomEleve;
    }

    public function setPrenomEleve(?string $prenomEleve): self
    {
        $this->prenomEleve = $prenomEleve;

        return $this;
    }

    public function getClasseEleve(): ?int
    {
        return $this->classeEleve;
    }

    public function setClasseEleve(?int $classeEleve): self
    {
        $this->classeEleve = $classeEleve;

        return $this;
    }

    public function getAnneeScolaire(): ?string
    {
        return $this->anneeScolaire;
    }

    public function setAnneeScolaire(?string $anneeScolaire): self
    {
        $this->anneeScolaire = $anneeScolaire;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?int
    {
        return $this->role;
    }
    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    public function setRole(?int $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getPresent(): ?int
    {
        return $this->present;
    }

    public function setPresent(?int $present): self
    {
        $this->present = $present;

        return $this;
    }
    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->login,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
}
