<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="idStage", type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usereleve")
     * @ORM\JoinColumn(name="idUserEleve", referencedColumnName="idUserEleve")
     */
    private $eleve;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Userprof")
     * @ORM\JoinColumn(name="idUserProf", referencedColumnName="idUserProf")
     */
    private $prof;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tuteur")
     * @ORM\JoinColumn(name="idTuteur", referencedColumnName="idTuteur", nullable=true)
     */
    private $tuteur;

    /**
     * @ORM\Column(name="dateStage", type="date", nullable=true)
     */
    private $dateStage;


    public function getId()
    {
        return $this->id;
    }

    public function getEleve()
    {
        return $this->eleve;
    }

    public function setEleve($eleve): self
    {
        $this->eleve = $eleve;

        return $this;
    }


    public function getProf()
    {
        return $this->prof;
    }

    public function setProf($prof): self
    {
        $this->prof = $prof;

        return $this;
    }


    public function getTuteur()
    {
        return $this->tuteur;
    }

    public function setTuteur($tuteur): self
    {
        $this->tuteur = $tuteur;

        return $this;
    }


    public function getDateStage(): ?\DateTimeInterface
    {
        return $this->dateStage;
    }

    public function setDateStage(?\DateTimeInterface $dateStage): self
    {
        $this->dateStage = $dateStage;

        return $this;
    }
}
